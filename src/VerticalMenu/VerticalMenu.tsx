import React from "react";
import classNames from "classnames";
import {useHistory, useLocation} from "react-router-dom";
import {IconName} from "@blueprintjs/icons";
import {Alignment, Button, Drawer, Position} from "@blueprintjs/core";
import {navigateTo, WIDTH_VERTICAL_MENU} from "../globals";
import './VerticalMenu.less';
import {getSelectionData, verticalMenuItems} from "./VerticalMenu.routing.select";

export const VerticalMenu: React.FC<IVerticalMenu> = ({isOpen, toggleMenuFn, isDrawerMode}) => (
  <div className={'vertical-menu-wrap'}>
    {isDrawerMode
      ? (
        <Drawer
          size={WIDTH_VERTICAL_MENU}
          isOpen={isOpen}
          position={Position.LEFT}
          usePortal={false}
          onClose={toggleMenuFn}
        >
          {menuObj.getElem()}
        </Drawer>
      )
      : (
        <div className={'vertical-menu-absolute'}>
          {menuObj.getElem()}
        </div>
      )}
  </div>
);

const Menu: React.FC<IMenu> = ({iconOnly, large}) => (
  <ul className={'vertical-menu'}>
    {verticalMenuItems.map(({pathname, text, buttonProps, submenu}) => (
      <MenuItem
        key={text}
        pathname={pathname}
        text={text}
        buttonProps={{...buttonProps, large}}
        iconOnly={iconOnly}
        submenu={submenu}
      />
    ))}
  </ul>
);
const menuObj: { elem?: any, getElem: () => any } = {
  getElem: function () {
    if (!this.elem) {
      this.elem = <Menu large={false} iconOnly={false}/>;
    }
    return this.elem;
  }
};

export const MenuItem: React.FC<IMenuItem> = ({pathname, text, buttonProps, iconOnly, submenu}) => {
  const history = useHistory();
  const location = useLocation();

  let activeClass = '';
  let isActiveBtn = false;
  let submenuElem: any = '';
  const selectionData = getSelectionData(location.pathname, pathname, submenu);
  if (!!selectionData && Object.keys(selectionData).length) {
    const {menuId, btnId} = selectionData;
    if (menuId === pathname) {
      activeClass = 'active';
      isActiveBtn = btnId === pathname;
      submenuElem = !!submenu ? <Submenu submenu={submenu} large={buttonProps.large}/> : '';
    }
  }

  return (
    <li className={classNames('vertical-menu__item', activeClass)}>
      <Button
        text={iconOnly ? undefined : text}
        alignText={Alignment.LEFT}
        minimal={true}
        fill={true}
        {...buttonProps}
        active={isActiveBtn}
        onClick={() => navigateTo({history, location}, pathname, {hideMenu: true})}
      />
      {submenuElem}
    </li>
  );
};

const Submenu: React.FC<ISubmenuProps> = ({submenu, large}) => {
  const history = useHistory();
  const location = useLocation();
  return (
    <ul className={'vertical-menu__submenu'}>
      {submenu.map(({text, pathname, buttonProps}, i) => {
          const selectionData = getSelectionData(location.pathname, pathname);
          return (
            <li key={i}>
              <Button
                text={text}
                icon={'blank'}
                alignText={Alignment.LEFT}
                minimal={true}
                fill={true}
                {...{...buttonProps, large}}
                active={selectionData ? selectionData.btnId === pathname : false}
                onClick={() => navigateTo({history, location}, pathname, {hideMenu: true})}
              />
            </li>
          );
        }
      )}
    </ul>
  );
};

interface IVerticalMenu {
  isOpen: boolean;
  toggleMenuFn: () => void;
  isDrawerMode: boolean;
}

interface IMenu {
  iconOnly?: boolean;
  large?: boolean;
}

interface IButtonProps {
  icon?: IconName;
  large?: boolean;
}

interface ISubmenuProps extends IButtonProps {
  submenu: ISubmenuItem[];
}

export interface ISelectData {
  menuId: string;
  btnId: string;
}

interface IButton {
  pathname: string;
  text: string;
  buttonProps: IButtonProps;
}

export interface IMenuItem extends IButton {
  iconOnly?: boolean;
  submenu?: ISubmenuItem[];
}

export interface ISubmenuItem extends IButton {
}
