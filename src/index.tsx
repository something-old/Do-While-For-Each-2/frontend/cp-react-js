import React from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import {App} from "./App/App";
import {BrowserRouter} from 'react-router-dom';

ReactDOM.render(
  <BrowserRouter basename={'/cp'}>
    <App/>
  </BrowserRouter>
  , document.getElementById('root'));
