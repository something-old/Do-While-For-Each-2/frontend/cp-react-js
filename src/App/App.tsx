import React, {useState} from 'react';
import {Redirect, Route, useLocation} from 'react-router-dom';
import {ControlPanel} from "../Cp/ControlPanel";
import {LOGIN_PATH} from "../globals";
import {Auth} from "../Auth/Auth";
import './App.less';

export const App: React.FC = () => {
  const location = useLocation();
  const [isLoggedIn, setLoggedIn] = useState(false);

  return (
    <Route
      render={(props) => {
        if (isLoggedIn) {
          return <ControlPanel/>;
        } else {
          if (location.pathname === LOGIN_PATH) {
            return <Auth/>
          } else {
            return <Redirect
              to={{
                pathname: LOGIN_PATH,
                state: {fromLocation: props.location}
              }}
            />
          }
        }
      }}
    />
  );
};

